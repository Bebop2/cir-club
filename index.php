
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Welcome on The game">
    <meta name="author" content="Valetin Bru et Jéremy Bonnefoi">

    <title>CIR-Club</title>

    <!-- Bootstrap core CSS -->
    <link href="./bootstrap-4.0.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/index.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
  </head>

  <body class="text-center">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
          <h3 class="masthead-brand">February</h3>
          <nav class="nav nav-masthead justify-content-center">
              <a class="nav-link active" href="./index.php">Home</a>
              <a class="nav-link" href="./pages/login.php">Sign-Up</a>
              <a class="nav-link" href="./pages/connexion.php">Connexion</a>
          </nav>
        </div>
      </header>

      <main role="main" class="inner cover">
        <h1 class="cover-heading">February Project.</h1>
        <p class="lead">This game is our Php February Project.</p>
        <p class="lead">
            <a href="./pages/connexion.php" class="btn btn-lg btn-secondary">Play</a>
        </p>
      </main>

      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>Valentin Bru et Jérémy Bonnefoi</p>
        </div>
      </footer>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="./bootstrap-4.0.0/assets/js/vendor/popper.min.js"></script>
    <script src="./bootstrap-4.0.0/dist/js/bootstrap.min.js"></script>
  </body>
</html>
